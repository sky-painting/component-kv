package com.coderman.component.kv.web.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.coderman.component.kv.api.KVApi;
import com.coderman.component.kv.model.KVGroupPair;
import com.coderman.component.kv.model.KVPairBO;
import com.coderman.component.kv.model.KVParentPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@RestController
public class KVController {
    @Resource(name = "kVApiImpl")
    private KVApi kvApi;

    /**
     * 获取所有kv对象
     * @return
     */
    @RequestMapping(value = "/get")
    public String get(){
        return JSON.toJSONString(kvApi.getAllList());
    }

    /**
     * 初始化数据使用
     * @return
     */
    @RequestMapping(value = "/initlist")
    public String init(){

        List<KVPairBO> kvPairBOList = new ArrayList<>();
        kvPairBOList.add(KVPairBO.instance("1","男").groupKey("GENDER_ENUM"));
        kvPairBOList.add(KVPairBO.instance("2","女").groupKey("GENDER_ENUM"));

        kvPairBOList.add(KVPairBO.instance("1","XX集团").groupKey("DEPART_INSTANCE").parentKey("0"));
        kvPairBOList.add(KVPairBO.instance("2","XX部门-1").groupKey("DEPART_INSTANCE").parentKey("1"));
        kvPairBOList.add(KVPairBO.instance("3","XX部门-1").groupKey("DEPART_INSTANCE").parentKey("1"));
        kvPairBOList.add(KVPairBO.instance("4","XX部门-2").groupKey("DEPART_INSTANCE").parentKey("2"));

        kvApi.batchInsert(kvPairBOList);

        return  "success";
    }

    /**
     * 初始化数据使用
     * @return
     */
    @RequestMapping(value = "/initreferlist")
    public String initRefer(){
        List<KVPairBO> kvPairBOList = new ArrayList<>();
        kvPairBOList.add(KVPairBO.instance("itemCount","10").referObj("YourOrder").referIdentifer("1"));
        kvPairBOList.add(KVPairBO.instance("itemOrder","202001020203023").referObj("YourOrder").referIdentifer("1"));
        kvPairBOList.add(KVPairBO.instance("shopId","20200102323").referObj("YourOrder").referIdentifer("1"));
        kvPairBOList.add(KVPairBO.instance("add1","北京市-朝阳区").referObj("YourOrder").referIdentifer("1").groupKey("address"));
        kvPairBOList.add(KVPairBO.instance("add2","杭州市-西湖区").referObj("YourOrder").referIdentifer("1").groupKey("address"));
        kvApi.batchInsert(kvPairBOList);

        return  "success";
    }


    /**
     * 初始化数据使用
     * @return
     */
    @RequestMapping(value = "/initjsonstr")
    public String initJsonStr(){
        Map<String,String> kvMap = new HashMap<>();
        kvMap.put("1","支付宝");
        kvMap.put("2","微信");
        kvMap.put("3","银行");
        KVPairBO jsonBo = KVPairBO.instance("payChannel",JSON.toJSONString(kvMap)).groupKey("PAY_SERVICE_CONFIG");
        kvApi.insert(jsonBo);
        return  "success";
    }

    /**
     * 初始化数据使用
     * @return
     */
    @RequestMapping(value = "/initjsonobj")
    public String initJsonObject(){
        KVPairBO kvJSONObject = KVPairBO.instance("json1","jsonvalue1").keyType(0).valueType(0).referObj("jsonObject1").referIdentifer("jsonCode1");
        KVPairBO jsonBo = KVPairBO.instance("jsonKey1","jsonValue1").valueObject(kvJSONObject);
        kvApi.insert(jsonBo);
        return  "success";
    }



    @RequestMapping(value = "/getgroup")
    public String getGroup(){
        List<KVGroupPair> kvParentPairList = kvApi.getByGroup("DEPART_INSTANCE");
        return  JSON.toJSONString(kvParentPairList);
    }

    @RequestMapping(value = "/getgroupmap")
    public String getGroupMap(){
        Map<String,List<KVParentPair>> kvMap = kvApi.getKVMapByGroupKey("DEPART_INSTANCE");
        return  JSON.toJSONString(kvMap);
    }

    @RequestMapping(value = "/getjsonobj")
    public String getJsonObject(){
        KVPairBO  valueJson = (KVPairBO) kvApi.getValueJson("jsonKey", KVPairBO.class);
        return JSON.toJSONString(valueJson);
    }


}
