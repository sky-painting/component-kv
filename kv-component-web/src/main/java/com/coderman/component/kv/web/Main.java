package com.coderman.component.kv.web;

import com.alibaba.fastjson.JSON;
import com.coderman.component.kv.model.KVPairBO;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

/**
 * Description:
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */

@SpringBootApplication(scanBasePackages = {"com.coderman.component.kv"})
@MapperScan(basePackages = "com.coderman.component.kv")
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }


}
