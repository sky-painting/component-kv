# component-kv


#### 痛点
1. 每一个业务系统或者业务服务中总有一些表存在扩展字段需求
2. 每个业务领域总有一些需求需要存字典类相关的数据结构，类似于业务配置对象，值对象,有些存储在数据库上，有些在配置服务上，
这里统一通过kv对象来收口管理
3. 这里统一通过一个KV场景相关的小模型来解决

#### 介绍
kv组件多场景实现，基于面向对象和DDD的思路扩展KV对象的使用场景
1. 支持简单的kv对象
2. 支持分组的kv对象
3. 支持父子级关系的kv对象
4. 支持引用主表和记录的扩展类kv对象
5. 支持value是json的kv对象

#### 组件功能架构图
![image](doc/component-kv组件.png)


#### 使用说明

1. 组件目前支持curd到数据库，使用者可以根据自己的实际情况复制组件内部代码做集成
2. 使用者也可以自己根据情况做二次开发来对接配置服务平台或者缓存等外部数据源

#### 参考文档
[mysql json字段对象解析](https://zhuanlan.zhihu.com/p/71147322)

[mysql json字段对象解析2](https://www.cnblogs.com/waterystone/p/5547254.html)

[mysql json对象解析封装组件](https://blog.csdn.net/u010173095/article/details/105763883/)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
