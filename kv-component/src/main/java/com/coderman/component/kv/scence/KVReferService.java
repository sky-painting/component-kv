package com.coderman.component.kv.scence;

import com.coderman.component.kv.model.KVGroupPair;
import com.coderman.component.kv.model.KVPairBO;
import com.coderman.component.kv.model.KVReferPair;
import com.coderman.component.kv.service.KVService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Description:
 * kv被引用的场景，建模是relation的意思，这里调整下
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class KVReferService {

    private KVService kvService;


    /**
     * 判断给定对象和标示是否存在引用的kv对象
     * @param referObj
     * @param referIdentifer
     * @return
     */
    public boolean contains(String referObj,String referIdentifer){
        return kvService.contains(KVPairBO.instance().referObj(referObj).referIdentifer(referIdentifer));
    }

    /**
     * 判断给定对象和标示+key是否存在引用的kv对象
     * @param key
     * @param referObj
     * @param referIdentifer
     * @return
     */
    public boolean contains(String key,String referObj,String referIdentifer){
        return kvService.contains(KVPairBO.instance(key,null).referObj(referObj).referIdentifer(referIdentifer));
    }

    /**
     * 根据引用对象和标示获取一条kv对象
     * @param referObj
     * @param referIdentifer
     * @return
     */
    public KVReferPair getOneByRefer(String referObj,String referIdentifer){
       return kvService.get(KVPairBO.instance().referIdentifer(referIdentifer).referObj(referObj)).convert2ReferPair();
    }

    /**
     * 获取多条数据
     * @param referObj
     * @param referIdentifer
     * @return
     */
    public List<KVReferPair> getListByRefer(String referObj,String referIdentifer){
        List<KVPairBO> kvPairBOList = kvService.getList(KVPairBO.instance().referIdentifer(referIdentifer).referObj(referObj));
        if(kvPairBOList == null || kvPairBOList.isEmpty()){
            return new ArrayList<>();
        }
        return kvPairBOList.stream().map(KVPairBO::convert2ReferPair).collect(Collectors.toList());
    }






}
