package com.coderman.component.kv.scence;

import com.coderman.component.kv.model.KVGroupPair;
import com.coderman.component.kv.model.KVPairBO;
import com.coderman.component.kv.service.KVService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Description:kv分组场景服务
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class KVGroupService{

    private KVService kvService;

    /**
     * 判断分组key下是否有指定key
     * @param key
     * @param groupKey
     * @return
     */
    public boolean contains(String key,String groupKey){
        return kvService.contains(KVPairBO.instance(key,null).groupKey(groupKey));
    }

    /**
     * 获取所有分组kv对象
     * @return
     */
    public Map<String, List<KVGroupPair>> getAllMap(){
        List<KVPairBO> kvPairBOList = kvService.getAll();
        List<KVGroupPair> kvGroupPairList = kvPairBOList.stream().map(KVPairBO::convert2GroupPair).collect(Collectors.toList());
        return kvGroupPairList.stream().collect(Collectors.groupingBy(KVGroupPair::getGStr));
    }

    /**
     * 根据一个分组key获取组内KV对象列表
     * @param groupKey
     * @return
     */
    public List<KVGroupPair> getByGroupKey(String groupKey){
        List<KVPairBO> kvPairBOList = kvService.getList(KVPairBO.instance().groupKey(groupKey));
        return kvPairBOList.stream().map(KVPairBO::convert2GroupPair).collect(Collectors.toList());
    }


}
