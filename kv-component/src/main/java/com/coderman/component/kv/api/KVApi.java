package com.coderman.component.kv.api;

import com.coderman.component.kv.model.KVGroupPair;
import com.coderman.component.kv.model.KVPairBO;
import com.coderman.component.kv.model.KVParentPair;

import java.util.*;

/**
 * Description:
 * date: 2022/1/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface KVApi {

    /**
     * 获取所有kv对象
     * @return
     */
    List<KVParentPair> getAllList();

    /**
     * 根据分组查询
     * @param groupKey
     * @return
     */
    List<KVParentPair> getByGroupWithParent(String groupKey);

    /**
     * 根据分组查询
     * @param groupKey
     * @return
     */
    List<KVGroupPair> getByGroup(String groupKey);


    /**
     * 根据分组查询
     * key:parent
     * @param groupKey
     * @return
     *  key:parentkey
     *  value:kvinstance
     */
    Map<String,List<KVParentPair>> getKVMapByGroupKey(String groupKey);

    /**
     * 根据某个key获取其当前key一直到父级key
     * @param key
     * @return
     */
    List<KVParentPair> getKeyWithParent(String key);

    /**
     * 批量插入数据
     * @param kvPairBOList
     * @return
     */
    boolean batchInsert(List<KVPairBO> kvPairBOList);

    /**
     * 插入单条数据
     * @param kvPairBO
     * @return
     */
    boolean insert(KVPairBO kvPairBO);


    /**
     * 根据key找到一个json value-->存储在value_json里面
     * @param key
     * @return
     */
    Object getValueJson(String key,Class<?> clazz);


    /**
     * 根据多条件查询是否存在KV数据
     * @param kvPairBO
     * @return
     */
    boolean contains(KVPairBO kvPairBO);


    /**
     * 通用场景的查询单条KV对象
     * @param kvPairBO
     * @return
     */
    KVPairBO getOne(KVPairBO kvPairBO);
    
    
}
