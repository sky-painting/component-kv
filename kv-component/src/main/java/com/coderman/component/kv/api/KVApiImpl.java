package com.coderman.component.kv.api;

import com.coderman.component.kv.model.KVGroupPair;
import com.coderman.component.kv.model.KVPairBO;
import com.coderman.component.kv.model.KVParentPair;
import com.coderman.component.kv.scence.KVGroupService;
import com.coderman.component.kv.scence.KVJSONService;
import com.coderman.component.kv.scence.KVParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Description:
 * date: 2022/1/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "kVApiImpl")
public class KVApiImpl implements KVApi{

    @Autowired
    private KVParentService kvParentService;

    @Autowired
    private KVJSONService kvjsonService;

    @Autowired
    private KVGroupService kvGroupService;


    @Override
    public List<KVParentPair> getAllList() {
        return kvParentService.getAllList();
    }

    @Override
    public List<KVParentPair> getByGroupWithParent(String groupKey) {
        return kvParentService.getByGroup(groupKey);
    }

    @Override
    public List<KVGroupPair> getByGroup(String groupKey) {
        return kvGroupService.getByGroupKey(groupKey);
    }

    @Override
    public Map<String, List<KVParentPair>> getKVMapByGroupKey(String groupKey) {
        return kvParentService.getKVMapByGroupKey(groupKey);
    }

    @Override
    public List<KVParentPair> getKeyWithParent(String key) {
        return kvParentService.getKeyWithParent(key);
    }

    @Override
    public boolean batchInsert(List<KVPairBO> kvPairBOList) {
        return kvParentService.batchInsert(kvPairBOList);
    }

    @Override
    public boolean insert(KVPairBO kvPairBO) {
        return kvParentService.insert(kvPairBO);
    }

    @Override
    public Object getValueJson(String key,Class<?> clazz) {
        return kvjsonService.getJsonObject(key, clazz);
    }

    @Override
    public boolean contains(KVPairBO kvPairBO) {
        return kvjsonService.contains(kvPairBO);
    }

    @Override
    public KVPairBO getOne(KVPairBO kvPairBO) {
        return kvjsonService.getOne(kvPairBO);
    }
}
