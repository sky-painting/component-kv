package com.coderman.component.kv.service;

import com.coderman.component.kv.dataobject.KVPairDO;
import com.coderman.component.kv.mapper.KVMapper;
import com.coderman.component.kv.model.KVPairBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Description:走数据库的配置服务
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "kVDBPersistService")
public class KVDBPersistService implements KVService {

    @Autowired
    private KVMapper kvMapper;

    @Override
    public List<KVPairBO> getAll() {
        List<KVPairDO> kvPairDOList = kvMapper.selectAll();
        if(kvPairDOList == null || kvPairDOList.isEmpty()){
            return new ArrayList<>();
        }
        return kvPairDOList.stream().map(KVPairBO::convertFromDO).collect(Collectors.toList());
    }

    @Override
    public KVPairBO get(KVPairBO kv) {
        return null;
    }

    @Override
    public List<KVPairBO> getList(KVPairBO kv) {
        List<KVPairDO> kvPairDOList = kvMapper.selectList(kv.convertToDO());
        return kvPairDOList.stream().map(KVPairBO::convertFromDO).collect(Collectors.toList());
    }

    @Override
    public boolean insert(KVPairBO kv) {
        int count = kvMapper.insert(kv.convertToDO());
        if(count > 0){
            return true;
        }
        return false;
    }

    @Override
    public boolean batchInsert(List<KVPairBO> kvList) {
        List<KVPairDO> kvDOList = kvList.stream().map(KVPairBO::convertToDO).collect(Collectors.toList());
        kvMapper.insertBatch(kvDOList);
        return true;
    }

    @Override
    public List<KVPairBO> getByKey(List<KVPairBO> kvList) {
        return null;
    }

    @Override
    public boolean update(KVPairBO kv) {
        return false;
    }

    @Override
    public boolean delete(KVPairBO kv) {
        return false;
    }

    @Override
    public boolean contains(KVPairBO kv) {
        return false;
    }
}
