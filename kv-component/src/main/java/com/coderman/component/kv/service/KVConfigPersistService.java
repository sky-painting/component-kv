package com.coderman.component.kv.service;

import com.coderman.component.kv.model.KVPairBO;

import java.util.List;

/**
 * Description:走远程配置服务的同步的持久化服务
 * date: 2022/1/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class KVConfigPersistService  implements KVService {

    @Override
    public List<KVPairBO> getAll() {
        return null;
    }

    @Override
    public KVPairBO get(KVPairBO kv) {
        return null;
    }

    @Override
    public List<KVPairBO> getList(KVPairBO kv) {
        return null;
    }

    @Override
    public boolean insert(KVPairBO kv) {
        return false;
    }

    @Override
    public boolean batchInsert(List<KVPairBO> kvList) {
        return false;
    }

    @Override
    public List<KVPairBO> getByKey(List<KVPairBO> kvList) {
        return null;
    }

    @Override
    public boolean update(KVPairBO kv) {
        return false;
    }

    @Override
    public boolean delete(KVPairBO kv) {
        return false;
    }

    @Override
    public boolean contains(KVPairBO kv) {
        return false;
    }
}
