CREATE TABLE `kv_instance` (
 `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
 `k` varchar(64) NOT NULL DEFAULT '' COMMENT 'key',
 `v` tinytext NOT NULL COMMENT 'value值',
 `value_json` json DEFAULT NULL,
 `group_key` varchar(64) DEFAULT '' COMMENT '分组key',
 `parent_key` varchar(64) DEFAULT '' COMMENT '父级key',
 `refer_key` varchar(64) DEFAULT '' COMMENT '引用对象key',
 `refer_id` varchar(64) DEFAULT '' COMMENT '引用对象实例ID',
 `value_type` int(11) DEFAULT '0' COMMENT 'value的Java数据类型',
 `key_type` int(11) DEFAULT '0' COMMENT 'key的Java数据类型',
 `status` int(11) DEFAULT NULL COMMENT '状态,跟着主对象走',
 PRIMARY KEY (`id`),
 KEY `idx_key` (`k`),
 KEY `idx_group_key` (`group_key`),
 KEY `idx_refer_id` (`refer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;